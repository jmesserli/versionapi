/**************************************************************************************************
 * version-api Copyright (C) 2014 Joel Messerli - JMNetwork.ch                                    *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package ch.jmnetwork.vapi.events;

import ch.jmnetwork.core.tools.file.FileUtils;

import java.io.File;
import java.util.ArrayList;


public class EventInformer {

    ArrayList<VapiListener> listeners = new ArrayList<VapiListener>();

    public void addListener(VapiListener vapiL) {

        listeners.add(vapiL);
    }

    public void informDownloadComplete() {

        for (VapiListener vl : listeners) {
            vl.downloadComplete();
        }

        FileUtils.deleteOnExit(new File("./tmp"));
    }

    public void informContentLengthAvialable(Long contentLength) {

        for (VapiListener vl : listeners) {
            vl.contentLengthAvialable(contentLength);
        }
    }

    public void informDownloadLengthUpdated(Long downloadedLength) {

        for (VapiListener vl : listeners) {
            vl.updateDownloadedLength(downloadedLength);
        }
    }
}
