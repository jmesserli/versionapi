/**************************************************************************************************
 * version-api Copyright (C) 2014 Joel Messerli - JMNetwork.ch                                    *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package ch.jmnetwork.vapi.network;

import ch.jmnetwork.vapi.util.PropertiesHandler;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


public class NetworkHelper {

    PropertiesHandler ph;
    String propertiesLocation;
    String Location;
    long contentLength;

    public NetworkHelper(String location) {

        Location = location;
        propertiesLocation = location + "versionApiInfo.xml";
        ph = new PropertiesHandler(getFileFromURL(propertiesLocation, "tmp\\versionApiInfo.xml"));
    }

    public long testConnectionAndGetContentLength(String URL) {

        URL website;
        HttpURLConnection httpuc;

        try {
            website = new URL(URL);
            try {
                httpuc = (HttpURLConnection) website.openConnection();
                contentLength = httpuc.getContentLength();
                httpuc.disconnect();

                return contentLength;
            } catch (IOException e1) {
                System.out.println();
                System.out.println("CANT OPEN CONNECTION TO " + website.getPath());
                e1.printStackTrace();
            }
        } catch (Exception e) {

        }

        return 0;
    }

    @Deprecated
    public File getFileFromURL(String URL, String saveLocation) {

        URL website;
        HttpURLConnection httpuc;
        ReadableByteChannel rbc;
        FileOutputStream fos;
        File f = new File(saveLocation);

        try {
            website = new URL(URL);
            try {
                httpuc = (HttpURLConnection) website.openConnection();
                contentLength = httpuc.getContentLength();
            } catch (IOException e1) {
                System.out.println();
                System.out.println("CANT OPEN CONNECTION TO " + website.getPath());
                e1.printStackTrace();
            }
            //System.out.println("[get file] " + website + " --> " + f.getAbsolutePath());
            rbc = Channels.newChannel(website.openStream());
            if (!f.exists()) {
                try {
                    File parentFile = null;
                    if (f.getParentFile() != null) {
                        parentFile = f.getParentFile();
                    }
                    if (!(parentFile == null)) {
                        parentFile.mkdirs();
                    }
                } catch (Exception e) {
                }
                f.createNewFile();
            }
            fos = new FileOutputStream(saveLocation);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

            fos.close();
        } catch (Exception e) {
            System.out.println("Beim Herunterladen von " + URL + " ist ein Fehler aufgetreten.");
            e.printStackTrace();
        }

        return new File(saveLocation);
    }

    /**
     * @param URL          file URL
     * @param saveLocation filename with path to the end save file
     * @return the saved file
     */
    public FileDownloader saveURLto(String URL, String saveLocation) {

        URL website;
        HttpURLConnection httpuc = null;
        FileOutputStream fos;
        File f = new File(saveLocation);
        FileDownloader fd = null;

        try {
            website = new URL(URL);
            try {
                httpuc = (HttpURLConnection) website.openConnection();
                contentLength = httpuc.getContentLength();
            } catch (IOException e1) {
                System.out.println();
                System.out.println("CANT OPEN CONNECTION TO " + website.getPath());
                e1.printStackTrace();
            }
            //System.out.println("[get file] " + website + " --> " + f.getAbsolutePath());
            if (!f.exists()) {
                try {
                    File parentFile = null;
                    if (f.getParentFile() != null) {
                        parentFile = f.getParentFile();
                    }
                    if (!(parentFile == null)) {
                        parentFile.mkdirs();
                    }
                } catch (Exception e) {
                }
                f.createNewFile();
            }
            fos = new FileOutputStream(saveLocation);

            fd = new FileDownloader(new BufferedInputStream(httpuc.getInputStream()), fos, contentLength);
            new Thread(fd).start();
        } catch (Exception e) {
            System.out.println("Beim Herunterladen von " + URL + " ist ein Fehler aufgetreten.");
            e.printStackTrace();
        }

        return fd;
    }

    public String getNewestVersion() {

        return ph.getProperty("NEWEST_VERSION", "NULL");
    }

    /**
     * @param zipFile
     * @param outputFolder
     * @deprecated please use the unzip method from VersionAPI
     */
    @Deprecated
    public void unZip(String zipFile, String outputFolder) {

        byte[] buffer = new byte[1024];
        try {
            File folder = new File(outputFolder);
            if (!folder.exists()) {
                folder.mkdir();
            }

            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));

            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {

                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);

                //System.out.println("[unzip] " + newFile.getAbsoluteFile());

                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();

//            System.out.println("[unzip] done");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getVersioningURL() {

        return Location;
    }

    public class FileDownloader implements Runnable {

        private long downloadedSize = 0;
        private BufferedInputStream bis;
        private FileOutputStream fos;
        private boolean downloadComplete = false;
        private long contentLength;

        byte data[] = new byte[1024];
        int count;

        public FileDownloader(BufferedInputStream inputstream, FileOutputStream outputStream, long contentLength) {

            bis = inputstream;
            fos = outputStream;
            this.contentLength = contentLength;
        }

        @Override
        public void run() {

            try {

                while ((count = bis.read(data, 0, 1024)) != -1) {
                    fos.write(data, 0, count);
                    downloadedSize += count;
                }

                finish();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bis != null && fos != null) {
                    try {
                        bis.close();
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        private void finish() {

            //System.err.println("FileDownloaderComplete");
            downloadComplete = true;
        }

        public long getDownloadedSize() {

            return downloadedSize;
        }

        public long getContentLength() {
            return contentLength;
        }

        public boolean isComplete() {

            return downloadComplete;
        }
    }
}
