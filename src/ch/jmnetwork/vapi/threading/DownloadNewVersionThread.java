/**************************************************************************************************
 * version-api Copyright (C) 2014 Joel Messerli - JMNetwork.ch                                    *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package ch.jmnetwork.vapi.threading;

import ch.jmnetwork.core.tools.file.FileUtils;
import ch.jmnetwork.vapi.VersionApi;
import ch.jmnetwork.vapi.network.NetworkHelper;

import java.io.File;


public class DownloadNewVersionThread implements Runnable {

    VersionApi versionApi;

    String downloadVersion, unzipLoc;
    DownloadingThread dlt;
    File temp_downloadfile;

    Boolean cL_available = false;

    public DownloadNewVersionThread(String versionToDownload, String unzipLocation, NetworkHelper networkHelper, VersionApi vapi) {
        downloadVersion = versionToDownload;
        unzipLoc = unzipLocation;
        versionApi = vapi;
        //dlt = new DownloadingThread(String.format("%sversions/%s.zip", Reference.LOCATION, downloadVersion), "tmp/versionDownloadTmp.zip", networkHelper, vapi);

    }

    @Override //TODO USE PROPPER LOGGING
    public void run() {

        System.out.println("[get] Version " + downloadVersion);
        new Thread(dlt).start();

        cL_available = true;
        while (!dlt.downloadComplete()) {
            System.out.println("[get] download not yet complete... waiting 0.5 s.");
            try {
                Thread.sleep(500);
                versionApi.getEventInformer().informDownloadLengthUpdated(dlt.getDownloadedSize());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        temp_downloadfile = dlt.getDownloadedFile();

        VersionApi.unzip("tmp/versionDownloadTmp.zip", unzipLoc);

        System.out.println("[get] Version " + downloadVersion + " [done]");
        versionApi.getEventInformer().informDownloadComplete();

        FileUtils.deleteDir(temp_downloadfile);
    }
}
