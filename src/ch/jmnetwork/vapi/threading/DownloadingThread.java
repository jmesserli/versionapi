/**************************************************************************************************
 * version-api Copyright (C) 2014 Joel Messerli - JMNetwork.ch                                    *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package ch.jmnetwork.vapi.threading;

import ch.jmnetwork.vapi.VersionApi;
import ch.jmnetwork.vapi.network.NetworkHelper;
import ch.jmnetwork.vapi.network.NetworkHelper.FileDownloader;

import java.io.File;


public class DownloadingThread implements Runnable {

    String fileToDownload;
    String saveLocation;
    NetworkHelper netHelper;
    File downloadedFile = null;
    Boolean ran = false;
    Boolean cLAvialable = false;
    Long contentLength = 0L;
    VersionApi vapi;
    FileDownloader fd;
    long downloadedSize = 0;

    public DownloadingThread(String fileURL, String saveLoc, String netHelperLocation, VersionApi vapi) {

        fileToDownload = new String(fileURL);
        saveLocation = new String(saveLoc);
        netHelper = new NetworkHelper(netHelperLocation);
        this.vapi = vapi;
    }

    public DownloadingThread(String fileURL, String saveLoc, NetworkHelper networkHelper, VersionApi vapi) {

        fileToDownload = new String(fileURL);
        saveLocation = new String(saveLoc);
        netHelper = networkHelper;
        this.vapi = vapi;
    }

    @Override
    public void run() {

        contentLength = netHelper.testConnectionAndGetContentLength(fileToDownload);
        cLAvialable = true;

        vapi.getEventInformer().informContentLengthAvialable(contentLength);

        fd = netHelper.saveURLto(fileToDownload, saveLocation);

        System.out.println(fd);

        while (!fd.isComplete()) {
            downloadedSize = fd.getDownloadedSize();
            System.out.println("FDnotComplete");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
        }

        ran = true;
        System.out.println("DLThread Complete");
    }

    /**
     * @return contentlength of the file currently downloading
     * @deprecated use vapilistener to get the contentlength
     */
    public long getContentLength() {

        return contentLength;
    }

    /**
     * @return if contentLength has been set
     * @deprecated
     */
    public boolean isContentLengthAvialable() {

        return cLAvialable;
    }

    /**
     * @return if the download has completed.
     */
    public boolean downloadComplete() {

        return ran;
    }

    public long getDownloadedSize() {

        return fd.getDownloadedSize();
    }

    /**
     * @return Returns the file downloaded or null if file did not get downloaded yet - check using {@link #downloadComplete()}
     */
    public File getDownloadedFile() {

        if (downloadComplete()) {
            return downloadedFile == null ? new File(saveLocation) : downloadedFile;
        } else {
            return null;
        }
    }
}
