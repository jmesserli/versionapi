/**************************************************************************************************
 * version-api Copyright (C) 2014 Joel Messerli - JMNetwork.ch                                    *
 *                                                                                                *
 *     This program is free software: you can redistribute it and/or modify                       *
 *     it under the terms of the GNU General Public License as published by                       *
 *     the Free Software Foundation, either version 3 of the License, or                          *
 *     (at your option) any later version.                                                        *
 *                                                                                                *
 *     This program is distributed in the hope that it will be useful,                            *
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of                             *
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                              *
 *     GNU General Public License for more details.                                               *
 *                                                                                                *
 *     You should have received a copy of the GNU General Public License                          *
 *     along with this program.  If not, see [http://www.gnu.org/licenses/].                      *
 **************************************************************************************************/

package ch.jmnetwork.vapi;

import ch.jmnetwork.core.logging.Logger;
import ch.jmnetwork.core.tools.zip.ZipUtils;
import ch.jmnetwork.vapi.events.EventInformer;
import ch.jmnetwork.vapi.events.VapiListener;
import ch.jmnetwork.vapi.lib.Reference;
import ch.jmnetwork.vapi.network.NetworkHelper;
import ch.jmnetwork.vapi.threading.DownloadNewVersionThread;
import ch.jmnetwork.vapi.util.PropertiesWorker;

import java.io.File;

public class VersionApi {
    public static Logger vapi_logger = new Logger(new File("versionapi.log"), "VersionAPI");

    NetworkHelper netHelper;
    PropertiesWorker pw;
    EventInformer ei = new EventInformer();

    public VersionApi(String propertiesLocation) {

        Reference.PROPERTIES_LOCATION = propertiesLocation;

        pw = new PropertiesWorker(Reference.PROPERTIES_LOCATION);
        pw.readProperties();

        netHelper = new NetworkHelper(Reference.LOCATION);
        // ...
        pw.setProperties();
        pw.saveProperties();
    }


    public static void unzip(String zipFileLoc, String outputFolder) {

        String source = zipFileLoc;
        String destination = outputFolder;

        ZipUtils.unzipAll(ZipUtils.getZipFileFromFile(zipFileLoc), new File(outputFolder));
    }

    public boolean isNewestVersion(String version) {

        Boolean isNewestVersion;
        isNewestVersion = netHelper.getNewestVersion().equalsIgnoreCase(version);
        return isNewestVersion;
    }

    public void downloadNewVersion(String versionToDownload, String unzipLocation) {

        DownloadNewVersionThread dlnvt = new DownloadNewVersionThread(versionToDownload, unzipLocation, netHelper, this);

        new Thread(dlnvt).start();
    }

    public String getNewestVersion() {

        return netHelper.getNewestVersion();
    }

    public String getVersioningURL() {

        return netHelper.getVersioningURL();
    }

    public void setVersioningURL(String newVersioningURL) {

        pw.getPropertiesHandler().setProperty("LOCATION", newVersioningURL);
        pw.getPropertiesHandler().saveProperties();
        //System.out.println("[version url] (new) " + newVersioningURL);
    }

    public boolean isVersioningURLSet() {
        return !netHelper.getVersioningURL().equalsIgnoreCase("nolocation");
    }

    public void addListener(VapiListener vl) {

        ei.addListener(vl);
    }

    public EventInformer getEventInformer() {

        return ei;
    }

    public void reload() {

        pw = new PropertiesWorker(Reference.PROPERTIES_LOCATION);
        pw.readProperties();

        netHelper = new NetworkHelper(Reference.LOCATION);
        // ...
        pw.setProperties();
        pw.saveProperties();
    }
}